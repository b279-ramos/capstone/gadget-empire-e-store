const User = require("../models/user");
const bcrypt =  require("bcryptjs");
const jwt = require("jsonwebtoken");

async function signup(req, res) {
    try {
        const {email, password} = req.body;
        const hashPassword = bcrypt.hashSync(password, 8)
        await User.create({ email, password:hashPassword });
        res.sendStatus(200);
    } catch (error) {
        console.log(error);
        res.sendStatus(400);
    }
}

async function login(req, res) {
    try {
        const {email,password} = req.body;

        const user = await User.findOne({email});
        if(!user) return res.sendStatus(401);

        const matchPassword = bcrypt.compareSync(password, user.password);
        if(!matchPassword) return res.sendStatus(401);

        const exp = Date.now() + 1000 * 60 * 60 * 24 * 30; // you can modify how many days or hrs for token expiration.
        //const exp = Date.now() + 1000 * 10; //valid for 10s

        const token = jwt.sign({sub: user._id, exp}, process.env.SECRET);
        
        res.cookie("Authorization", token, {
            expires: new Date(exp),
            httpOnly: true,
            sameSite: "lax",
            secure: process.env.NODE_ENV === "production",
        });
        res.sendStatus(200);
    } catch (error) {
        console.log(error);
        res.sendStatus(401);
    }
    
}

function logout(req, res) {
    console.log("here");
    res.clearCookie("Authorization");
    res.sendStatus(200);
}

function checkAuth(req, res) {
    try {
        console.log(req.user);
        res.sendStatus(200);
    } catch (error) {
        console.log(error);
        res.sendStatus(401);
    }
    
}

module.exports = {
    signup,
    login,
    checkAuth,
    logout
}