const jwt = require("jsonwebtoken");
const User = require("../models/user");

async function requireAuth(req, res, next) {
    try {
        const token = req.cookies.Authorization;
        const decoded = jwt.verify(token, process.env.SECRET)
        
        if(Date.now() > decoded.exp) return res.sendStatus(401); //check if token expired

        const user = await User.findById(decoded.sub); // check if user exists
        if (!user) return res.sendStatus(401); 
        
        req.user = user;
        next();
    } catch (error) {
        res.sendStatus(401);
    }
}

module.exports = requireAuth;