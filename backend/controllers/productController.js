const Product = require("../models/product");

const createProduct = async (req, res) => {
    try {
        // data from req.body
        const { 
            product_name, 
            description,
            price,
            qty,
        } = req.body

        // create product
        const product = await Product.create({
            product_name,
            description,
            price,
            qty
        });

        // response with the new note
        res.json({product});
    } catch (error) {
        console.log(error);
        res.sendStatus(401);
    }
}

const getAllProduct = async (req, res) => {
    try {

        const products = await Product.find({ isDeleted: { $ne: 1 } });
        res.json({products});

    } catch (error) {
        console.log(error);
        res.sendStatus(401);
    }
}

const getOneProduct = async (req, res) => {
    try {
        // get product using id
        const productId = req.params.id;

        // find the product using the given id
        const product = await Product.findById(productId);

        // respond the note using id
        res.json({product});

    } catch (error) {
        console.log(error);
        res.sendStatus(401);
    }
}

const updateProduct = async (req, res) => {

    try {

        const productId = req.params.id;
        const { 
            product_name, 
            description,
            price,
            qty,
        } = req.body

        await Product.findByIdAndUpdate(productId, {
            product_name,
            description,
            price,
            qty
        });

        const product = await Product.findById(productId);

        // respond the note using id
        res.json({product});

    } catch (error) {
        console.log(error);
        res.sendStatus(401);
    }
}

const deleteProduct = async (req, res) => {
    try {
        
        const productId = req.params.id;
        await Product.findByIdAndUpdate(productId, {
            isDeleted : 1
        });
        const product = await Product.findById(productId);
        res.json({product});

    } catch (error) {

        console.log(error);
        res.sendStatus(401);
    }
}

module.exports = {
    createProduct,
    getAllProduct,
    getOneProduct,
    updateProduct,
    deleteProduct
}