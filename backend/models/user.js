const { mongo, default: mongoose } = require("mongoose");

const userSchema = new mongoose.Schema({
    email : {
		type : String,
		required: true,
		unique: true,
		lowercase: true,
		index: true,
	},
    password : {
		type : String,
		required: true,
	},
    isAdmin : {
		type : Boolean,
		default: 0
	},
    created_at : {
        type : Date,
        default : new Date()
    },
});

const User = mongoose.model("User", userSchema);
module.exports = User;