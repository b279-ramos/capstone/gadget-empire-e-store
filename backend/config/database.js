if(process.env.NODE_ENV != "production") {
    require("dotenv").config();
}

const mongoose = require("mongoose");

async function database() {
    try {
        await mongoose.connect(process.env.DATABASE_URL);
        console.log("Connected to the Database");
    } catch (error) {
        console.log(error);
    }
}

module.exports = database